<?php
 
namespace App\Conversations;

use BotMan\BotMan\Facades\BotMan;
use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Question as BotManQuestion;
use BotMan\BotMan\Messages\Incoming\Answer as BotManAnswer;
use Illuminate\Support\Facades\Http;

class ExampleConversation extends Conversation
{
    protected $jawaban = 'pegawai';
    
    /**
     * First question
     */
    public function askHadits()
    {
        $question = Question::create("Apakah anda ingin melakukan pelayanan Kepegawaian atau pelayanan Kejaksaan untuk Masyarakat Publik?")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                Button::create('Pelayanan Kepegawaian Kejaksaan')->value('pegawai'),
                Button::create('Pelayanan Masyarakat Publik')->value('masyarakat')
            ]);
 
        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                switch ($answer->getValue()) {
                    case 'pegawai':
                        $this->jawaban = 'pegawai';
                        $this->verifikasiPegawai('Saya Pegawai Kejaksaan');
                        break;
                    case 'masyarakat':
                        $this->jawaban = 'masyarakat';
                        $this->pemilihanLayanan('Saya Masyarakat Publik');
                        break;
                    default:
                        # code...
                       break;
                }
            }
        });
    }
 

    public function verifikasiPegawai($jawaban)
    {
        $this->say('Anda seorang pegawai Kejaksaan, kami memerlukan validasi data anda');
        $this->ask('Silahkan masukkan Nama Lengkap anda (tanpa gelar).', function (Answer $answer) {
            $nama = $answer->getText();
            $this->ask('Silahkan masukkan NIK anda (tanpa spasi).', function (Answer $answer) {
                $nik = $answer->getText();
                $this->ask('Silahkan masukkan NRP anda (tanpa spasi).', function (Answer $answer) {
                    $nrp = $answer->getText();
                    $this->ask('Silahkan masukkan NIP anda (tanpa spasi).', function (Answer $answer) {
                        $nip = $answer->getText();
                        $token = '$10$YVwGDLhBiQSrW8gbaOUmWOmULk8uUBjbm.cc8bX5dtxkt.kJIum9e';
                        $url = 'https://mysimkari.kejaksaan.go.id/api/pegawai/';
                        try {
                            $response = Http::withToken($token)->post($url, [
                                'nip' => $nip,
                            ]);
                            // $kata = $response['data']['nama'];
                            $kata = 'Data pegawai ditemukan';
                            // $kata = 'ok';
                        } catch (\GuzzleHttp\Exception\ConnectException $e) {
                            $kata = 'Terjadi kendala pada koneksi kami dengan database kepegawaian.';
                        }
                        if($response->getStatusCode() == '200'){
                            $this->say($kata);
                            $this->say('Nama Pegawai : '.$response['data']['nama'].". \r\t\n\n".'NIP Pegawai : '.$response['data']['nip'].". \r\t\n\n".'NRP Pegawai : '.$response['data']['nrp'].". \r\t\n\n".'Jenis Kelamin Pegawai : '.$response['data']['jk'].". \r\t\n\n".'Tanggal lahir : '.$response['data']['tgl_lahir'].". \r\t\n\n".'Usia Pegawai : '.$response['data']['usia'].". \r\t\n\n".'Alamat Pegawai : '.$response['data']['alamat_domisili'].". \r\t\n\n".'Agama Pegawai : '.$response['data']['agama'].". \r\t\n\n".'Jabatan Pegawai : '.$response['data']['jabatan'].". \r\t\n\n".'Golongan Pangkat Pegawai : '.$response['data']['golpang'].". \r\t\n\n".'Kelas Jabatan Pegawai : '.$response['data']['kelas_jabatan'].". \r\t\n\n".'Satker Pegawai : '.$response['data']['nama_satker']);
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                        }else{
                            $this->say($kata);
                        }
                    });
                });
            });
        });
    }
    
    

    public function pemilihanLayanan($jawaban)
    {
        
        $question = Question::create("Kami memiliki beberapa pelayanan untuk masyarakat publik silahkan pilih salah satu layanan kami.")
            ->fallback('Unable to ask question')
            ->callbackId('ask_reason')
            ->addButtons([
                // Button::create('Informasi Perkara Pidum')->value('infoPidum'),
                // Button::create('Informasi Perkara Pidsus')->value('infoPidsus'),
                // Button::create('Informasi Tilang')->value('infoTilang'),
                // Button::create('Informasi Tahanan')->value('infoTahanan'),
                Button::create('Permohonan Antar Barang Bukti')->value('infoAntarBarangBukti'),
                Button::create('Permohonan Antar Tilang')->value('infoAntarTilang'),
                Button::create('Permohonan Antar Jemput Saksi Anak/Difabel')->value('infoAntarJemputSaksiAnakDifabel'),
                Button::create('Permohonan Pinjam Barang Bukti')->value('infoPinjamBarangBukti'),
                Button::create('Permohonan Penjengukan Tahanan')->value('infoJengukTahanan'),
                Button::create('Permohonan Penyuluhan Hukum')->value('infoPenyuluhanHukum'),
                Button::create('Permohonan Pendampingan Hukum')->value('infoPendampinganHukum'),
                Button::create('Permohonan Konsultasi Hukum')->value('infoKonsultasiHukum'),
                Button::create('Permohonan Bantuan Hukum')->value('infoBantuanHukum'),
                Button::create('Pengawasan Aliran Kepercayaan Masyarakat')->value('infoPakem'),
                Button::create('Pelaporan Organisasi Masyarakat')->value('infoOrmas'),
                Button::create('Pengamanan Pembangunan Strategis')->value('infoPps'),
                Button::create('Pengaduan Tindak Pidana Korupsi')->value('infoPengaduan'),
                Button::create('Cek status terakhir Permohonan / Pengaduan')->value('cekStatus'),
        ]);
        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                switch ($answer->getValue()) {
                    // case 'infoPidum':
                        
                    // break;
                    // case 'infoPidsus':
                        
                    // break;
                    // case 'infoTilang':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    case 'infoAntarBarangBukti':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Antar Barang Bukti, Permohonan Antar Barang Bukti bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Antar Barang Bukti dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                        $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                        ->fallback('Unable to ask question')
                        ->callbackId('ask_reason')
                        ->addButtons([
                            Button::create('Ya')->value('ya'),
                            Button::create('Tidak')->value('tidak'),
                        ]);
                        return $this->ask($question, function (Answer $answer) {
                            if ($answer->isInteractiveMessageReply()) {
                                switch ($answer->getValue()) {
                                    case 'ya':
                                        $this->pemilihanLayanan('Saya Masyarakat Publik');
                                    break;
                                    case 'tidak':
                                        $this->thankyou('Thankyou');
                                    break;
                                }
                            }
                        });
                    break;
                    case 'infoAntarTilang':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Antar Tilang, Permohonan Antar Tilang bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Antar Tilang dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                        $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                        ->fallback('Unable to ask question')
                        ->callbackId('ask_reason')
                        ->addButtons([
                            Button::create('Ya')->value('ya'),
                            Button::create('Tidak')->value('tidak'),
                        ]);
                        return $this->ask($question, function (Answer $answer) {
                            if ($answer->isInteractiveMessageReply()) {
                                switch ($answer->getValue()) {
                                    case 'ya':
                                        $this->pemilihanLayanan('Saya Masyarakat Publik');
                                    break;
                                    case 'tidak':
                                        $this->thankyou('Thankyou');
                                    break;
                                }
                            }
                        });
                    break;
                    case 'infoAntarJemputSaksiAnakDifabel':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Antar Jemput Saksi Anak/Difabel, Permohonan Antar Jemput Saksi Anak/Difabel bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Antar Jemput Saksi Anak/Difabel dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                        $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                        ->fallback('Unable to ask question')
                        ->callbackId('ask_reason')
                        ->addButtons([
                            Button::create('Ya')->value('ya'),
                            Button::create('Tidak')->value('tidak'),
                        ]);
                        return $this->ask($question, function (Answer $answer) {
                            if ($answer->isInteractiveMessageReply()) {
                                switch ($answer->getValue()) {
                                    case 'ya':
                                        $this->pemilihanLayanan('Saya Masyarakat Publik');
                                    break;
                                    case 'tidak':
                                        $this->thankyou('Thankyou');
                                    break;
                                }
                            }
                        });
                    break;
                    case 'infoPinjamBarangBukti':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Pinjam Barang Bukti, Permohonan Pinjam Barang Bukti bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Pinjam Barang Bukti dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoJengukTahanan':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Jenguk Tahanan, Permohonan Jenguk Tahanan bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Jenguk Tahanan dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoPenyuluhanHukum':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Permohonan Penyuluhan hukum, Permohonan Permohonan Penyuluhan hukum bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Permohonan Penyuluhan hukum dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoPendampinganHukum':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Permohonan Pendampingan Hukum, Permohonan Permohonan Pendampingan Hukum bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Permohonan Pendampingan Hukum dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoKonsultasiHukum':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Permohonan Pendampingan Hukum, Permohonan Permohonan Pendampingan Hukum bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Permohonan Pendampingan Hukum dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoBantuanHukum':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Permohonan Bantuan Hukum, Permohonan Permohonan Bantuan Hukum bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Permohonan Bantuan Hukum dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoPakem':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Pelaporan Pengawasan Aliran Kepercayaan Masyarakat, Permohonan Pelaporan Pengawasan Aliran Kepercayaan Masyarakat bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Pelaporan Pengawasan Aliran Kepercayaan Masyarakat dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoOrmas':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Pelaporan Organisasi Masyarakat, Permohonan Pelaporan Organisasi Masyarakat bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Pelaporan Organisasi Masyarakat dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'infoPps':
                        $this->say('Kejaksaan Republik Indonesia saat ini memiliki pelayanan Permohonan Pendampingan Pembangunan Strategis, Permohonan Permohonan Pendampingan Pembangunan Strategis bisa anda dapatkan pada aplikasi kami di alamat https://pelayanan.kejaksaan.go.id, jika anda sudah mengunjungi aplikasi tersebut silahkan klik menu Permohonan Pendampingan Pembangunan Strategis dan isikan semua inputan yang tersedia. Anda juga tiap saat dapat melihat perkembangan dari permohonan pelayanan anda pada aplikasi tersebut.');
                            $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                            ->fallback('Unable to ask question')
                            ->callbackId('ask_reason')
                            ->addButtons([
                                Button::create('Ya')->value('ya'),
                                Button::create('Tidak')->value('tidak'),
                            ]);
                            return $this->ask($question, function (Answer $answer) {
                                if ($answer->isInteractiveMessageReply()) {
                                    switch ($answer->getValue()) {
                                        case 'ya':
                                            $this->pemilihanLayanan('Saya Masyarakat Publik');
                                        break;
                                        case 'tidak':
                                            $this->thankyou('Thankyou');
                                        break;
                                    }
                                }
                            });
                    break;
                    case 'cekStatus':
                        $this->ask('Silahkan masukkan Nomor Berkas Permohonan / Pengaduan anda', function (Answer $answer) {
                            $noBerkas = $answer->getText();
                            $curl = curl_init();
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "https://api-dev.kejaksaan.go.id/token/api/auth/login?email=sipede@kejaksaan.go.id&password=5p3d32022!!?",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30000,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "POST",
                                CURLOPT_HTTPHEADER => array(
                                    "accept: */*",
                                    "accept-language: en-US,en;q=0.8",
                                    "content-type: application/json",
                                ),
                            ));

                            $response = curl_exec($curl);
                            $err = curl_error($curl);
                            $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                            curl_close($curl);
                            if($responCode == 200){
                                $data = json_decode($response);    
                                $token = $data->access_token;
                                $authorization = "Authorization: Bearer ".$token;
                                $curl = curl_init();
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "https://api-dev.kejaksaan.go.id/sipede-prod/api/ruang-berbagi/check-status?nomor=".$noBerkas,
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30000,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_HTTPHEADER => array(
                                        "accept: */*",
                                        "accept-language: en-US,en;q=0.8",
                                        "content-type: application/json",
                                        $authorization
                                    ),
                                ));

                                $response = curl_exec($curl);
                                $err = curl_error($curl);
                                $responCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                                if($responCode == 200){
                                    $data = json_decode($response);   
                                    $log = $data->data->suratMasukLog[0];
                                    $jenisLayanan = $data->data->hal;
                                    $tanggalLayanan = $data->data->tanggal;
                                    $this->say('Nomor Permohonan / Pengaduan : '.$noBerkas.' dengan perihal '.$jenisLayanan.' pada tanggal '.$tanggalLayanan.". \r\t\n\n".'Sedang dalam proses oleh pejabat '.$log->nama_jabatan);
                                    $question = Question::create("Apakah anda memerlukan layanan yang lain?.")
                                    ->fallback('Unable to ask question')
                                    ->callbackId('ask_reason')
                                    ->addButtons([
                                        Button::create('Ya')->value('ya'),
                                        Button::create('Tidak')->value('tidak'),
                                    ]);
                                    return $this->ask($question, function (Answer $answer) {
                                        if ($answer->isInteractiveMessageReply()) {
                                            switch ($answer->getValue()) {
                                                case 'ya':
                                                    $this->pemilihanLayanan('Saya Masyarakat Publik');
                                                break;
                                                case 'tidak':
                                                    $this->thankyou('Thankyou');
                                                break;
                                            }
                                        }
                                    });
                                }else{
                                    $this->say('No Berkas belum diverifikasi / tidak terdaftar dalam sistem');
                                }
                            }else{
                                $this->say('Terjadi kendala pada server PTSP');
                            }
                        }); 
                    break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;
                    // case 'infoTahanan':
                        
                    // break;

                    
                    default:
                        # code...
                       break;
                }
            }
        });
       
        

    }

    public function thankyou($jawaban)
    {
        $this->say('Terimakasih telah menggunakan layanan Kejaksaan pada saluran Telegram kami, jika ingin menggunakan layanan kembali silahkan ketik "/start" untuk Bot kami dapat melayani anda kembali');
    }


    public function jawabanNya($jawaban)
    {
        $this->ask('Kamu memilih kitab dari '.$jawaban.'. Silahkan masukkan nomor hadits yang ingin dicari.', function (Answer $answer) {
            $no = $answer->getText();
            // echo $no;
            $hasil=$this->getData($this->kitab, $no);
            $jawaban = sprintf("Hadits menjelaskan tentang: ".$hasil[3].". \r\t\n\n ".$hasil[0]."\r\t\n\n ".$hasil[1]);
            // $this->say('Hadits menjelaskan tentang: '.$hasil[3]);
            // $this->say($hasil[0]);
            $this->say($jawaban);
            if ($hasil[2]==true) {
                $this->say('Silahkan laporkan permasalahan ini dengan menu /lapor .');
            }
        });
    }
 
    public function getData($kitab, $no)
    {
        // try {
        //     $str='https://scrape-fastapi.herokuapp.com/hadits/?tokoh='.$jawaban.'&no='.$no;
        //     // $str='https://hadits-api-zhirrr.vercel.app/books/'.$kitab.'/'.$no;
        //     $dt = json_decode(file_get_contents($str));
        //     return [$dt->data->contents->arab, $dt->data->contents->id, false, $dt->data->contents->judul];
        // } catch (\Throwable $th) {
        //     return ["Something went wrong 😯️","Sepertinya ada masalah.🧐️", true];
        // }
 
    }
    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askHadits();
        // $this->cariLagi();
    }
}
 